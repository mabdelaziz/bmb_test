# -*- coding: utf-8 -*-

{
    'name': 'Courses',
    'version': '14.0.1.0.0',
    'category': 'tool',
    'summary': """
       Courses
    """,
    'author': 'Mahmoud Abdelaziz',
    'depends': ['base','website'],
    'data': [
        'security/ir.model.access.csv',
        'data/course_data.xml',
        'views/course_view.xml',
        'views/room_view.xml',
        'views/attendee_view.xml',
        'views/res_partner_view.xml',
        'views/course_portal_templates.xml',
    ],
    'installable': True,
    'license': 'AGPL-3',
}
