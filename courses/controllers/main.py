# -*- coding: utf-8 -*-

import collections
import babel.dates
import re
import werkzeug
from werkzeug.datastructures import OrderedMultiDict
from werkzeug.exceptions import NotFound

from ast import literal_eval
from collections import defaultdict
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, http, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.event.controllers.main import EventController
from odoo.http import request
from odoo.osv import expression
from odoo.tools.misc import get_lang, format_date


class WebsiteCourseController(http.Controller):

    @http.route('/course', type='http', auth="public", website=True)
    def course(self, **kwargs):
        course_ids = request.env['course.course'].search([])
        values = {
            'course_ids': course_ids.sudo(),

        }
        return request.render("courses.courses_page", values)
