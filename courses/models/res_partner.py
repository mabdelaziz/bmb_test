from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    instructor = fields.Boolean()
    course_ids = fields.One2many(comodel_name='course.course', inverse_name='instructor_id', string='Course')
