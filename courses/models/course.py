from odoo import fields, models, api, _
from odoo.exceptions import UserError


class Course(models.Model):
    _name = 'course.course'
    _description = 'Course'

    name = fields.Char(required=True)
    instructor_id = fields.Many2one('res.partner', domain=[('instructor', '=', True)], string='Instructor',
                                    required=True)
    room_id = fields.Many2one('course.room', string='Room', required=True)
    attendee_ids = fields.Many2many('course.attendee', string='Attendees')
    lesson_ids = fields.One2many(comodel_name='course.lesson', inverse_name='course_id', string=' lessons')
    lessons_count = fields.Integer(compute='_compute_lessons_count')
    available_seats = fields.Integer(compute='_compute_available_seats')
    date = fields.Date(required=True)
    description = fields.Text(required=True)

    @api.depends('room_id', 'attendee_ids')
    def _compute_available_seats(self):
        for rec in self:
            rec.available_seats = 0
            if rec.room_id and rec.attendee_ids:
                rec.available_seats = rec.room_id.capacity - len(rec.attendee_ids)

    @api.constrains('room_id', 'attendee_ids')
    @api.onchange('room_id', 'attendee_ids')
    def _check_available_seats(self):
        for rec in self:
            if rec.room_id and rec.attendee_ids:
                if len(rec.attendee_ids) > rec.room_id.capacity:
                    raise UserError(_('You cannot add any  new attendees because the is full.'))

    @api.depends('lesson_ids')
    def _compute_lessons_count(self):
        for rec in self:
            rec.lessons_count = 0
            if rec.lesson_ids:
                rec.lessons_count = len(rec.lesson_ids)


class Attendee(models.Model):
    _name = 'course.attendee'
    _description = 'Attendee'

    name = fields.Char(required=True)
    active = fields.Boolean(default=True)


class Lesson(models.Model):
    _name = 'course.lesson'
    _description = 'Lesson'

    name = fields.Char(required=True)
    course_id = fields.Many2one('course.course', string='Course')
    room_id = fields.Many2one('course.room', related='course_id.room_id', string='Room')


class Room(models.Model):
    _name = 'course.room'
    _description = 'Room'

    name = fields.Char(required=True)
    active = fields.Boolean(default=True)
    capacity = fields.Integer(required=True)
