# -*- coding: utf-8 -*-

{
    'name': 'Scheduled Reminders',
    'version': '14.0.1.0.0',
    'category': 'tool',
    'summary': """
       Scheduled reminders
    """,
    'author': 'Mahmoud Abdelaziz',
    'depends': ['base', 'base_automation', 'mail'],
    'data': [
        'security/ir.model.access.csv',
        'data/automation.xml',
        'views/scheduled_reminders.xml',
    ],
    'installable': True,
    'license': 'AGPL-3',
}
