from odoo import fields, models, api, _


class ScheduledReminders(models.Model):
    _name = 'scheduled.reminders'
    _description = 'Scheduled Reminders'

    name = fields.Char(required=True)
    date = fields.Datetime(required=True)
    partner_ids = fields.Many2many("res.partner", required=True, string="Recipient(s)")
    line_ids = fields.One2many("scheduled.reminders.line", 'scheduled_id', string="Recipient(s)")

    def send_scheduled_reminders_notification(self):
        for rec in self:
            body = '<br>'.join(set(rec.line_ids.mapped('name')))
            values = {
                'subject': rec.name,
                'body': body,
                'model': 'scheduled.reminders',
                'message_type': 'email',
                'res_id': rec.id,
                'partner_ids': [(6, 0, rec.partner_ids.ids)]
            }
            self.env['mail.message'].sudo().create(values)


class ScheduledRemindersLines(models.Model):
    _name = 'scheduled.reminders.line'
    _description = 'Reminders'

    scheduled_id = fields.Many2one("scheduled.reminders")
    name = fields.Char(required=True)
