from odoo import fields, models, api, _


class SurveyQuestion(models.Model):
    _inherit = 'survey.question'

    question_type = fields.Selection(selection_add=[('multiple_matrix', 'Multiple Matrix')])


class SurveyQuestion(models.Model):
    _inherit = 'survey.question.answer'

    question_type = fields.Selection([
        ('selection', 'Selection'),
        ('text_box', 'Text Input')], default='selection', string="Question Type")
    answer_ids = fields.Many2many('matrix.question.answer', string="Answers")


class MatrixQuestionAnswer(models.Model):
    _name = 'matrix.question.answer'

    name = fields.Char()
