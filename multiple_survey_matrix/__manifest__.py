# -*- coding: utf-8 -*-

{
    'name': 'Multiple Survey Matrix',
    'version': '14.0.1.0.0',
    'category': 'tool',
    'summary': """
       Cutome Survey Matrix
    """,
    'author': 'Mahmoud Abdelaziz',
    'depends': ['survey'],
    'data': [
        'security/ir.model.access.csv',
        'views/survey_question.xml',
    ],
    'installable': True,
    'license': 'AGPL-3',
}
